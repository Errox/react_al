import React from 'react';
import { Link } from 'react-router-dom';

function Nav() {
    return (
        <nav>
            <p>
                <Link to="/">     
                    Homepage | 
                </Link>
                <Link to="/primenumber">
                    Prime numbers |  
                </Link> 
                <Link to="/Pi">
                    Pi |
                </Link>
                <Link to="/Person">
                    Person |
                </Link>
                <Link to="/Photo">
                    Photo |
                </Link>
            </p>
        </nav>
    )
}

export default Nav; 
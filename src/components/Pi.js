import React from 'react';

class Pi extends React.Component {
    constructor() {
        super();
        this.state = {
            time: 0,
            amount: 0,
            piResult: 0
        };
    }
            
    ToInteger(x) {
        x = Number(x);
        return x < 0 ? Math.ceil(x) : Math.floor(x);
    }

    modulo(a, b) {
        return a - Math.floor(a / b) * b;
    }

    ToUint32(x) {
        return this.modulo(this.ToInteger(x), Math.pow(2, 32));
    }

    handleChange(amount){  
        let time = performance.now();
        let digits = 1000;

        digits++;

        let x = new Uint32Array(digits * 10 / 3 + 2)
        let r = new Uint32Array(digits * 10 / 3 + 2);

        let pi = new Uint32Array(digits);

        for (let j = 0; j < x.length; j++) {
        x[j] = 20;
        }

        for (let i = 0; i < digits; i++) {
            let carry = this.ToUint32(0);
            for (let j = 0; j < x.length; j++) {
                let num = this.ToUint32(x.length - j - 1);
                let dem = this.ToUint32(num * 2 + 1);

                x[j] += carry;

                let q = this.ToUint32(x[j] / dem);
                r[j] = x[j] % dem;

                carry = q * num;
            }

            pi[i] = (x[x.length - 1] / 10);

            r[x.length - 1] = x[x.length - 1] % 10;;

            for (let j = 0; j < x.length; j++) {
                    x[j] = r[j] * 10;
            }
        }

        let result = "";

        let c = this.ToUint32(0);

        for (let i = pi.length - 1; i >= 0; i--) {
            pi[i] += c;
            c = pi[i] / 10;

            result = (pi[i] % 10) + result;
        }

        this.piNumber = result;

        this.setState({ piResult: result, time: performance.now() - time });
    }

    render(){
        return (
            <div >
                <h1> Pi </h1>
                <p> Pi result: {this.state.piResult} </p>
                <p> Pi Time: {this.state.time.toString().substr(0, 4) + " milliseconds"} </p>
                {<button onClick={(e) => this.handleChange(1000000000)}>
                    calculate with 1m iterations
                </button>}
            </div>
        );
    }
}
export default Pi;

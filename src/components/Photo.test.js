import React from 'react';
import { render } from '@testing-library/react';
import Photo from './Photo';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Data from './../assets/data.json'

Enzyme.configure({ adapter: new Adapter() });

test('Should create h1 text', () => {
    const { getByText } = render(<Photo />);
    const linkElement = getByText("Photo");
    expect(linkElement).toBeInTheDocument();
});


it('Should contain image', () => {
    const wrapper = Enzyme.shallow(<Photo />);
    const firstButton = wrapper.find('button').at(0);
  
    firstButton.simulate('click');
    setTimeout(() => {
      expect(wrapper.state()).time.not(0)
    }, 5000);
});

import React from 'react';

class Person extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            error : null,
            isLoaded : false,
            posts : [],
            time: Date          
        };
    }

    loadPerson(){ 
        let time = performance.now();
        fetch("https://al-test-api.herokuapp.com/data")
        .then( response => response.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded : true,
                    persons : result,
                    time: performance.now() - time
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error,
                    time: performance.now() - time
                })
            },
        )
    }

    render() {
        const {error, isLoaded, persons} = this.state;

        if(error){
            return <div>Error in loading</div>
        }else if (!isLoaded) {
            return <div><h1> Load json </h1><button onClick = {(e) => this.loadPerson()}> Press </button></div>
        }else{
            return(
                <div>
                    <h1> Load json </h1>
                    <p>{this.state.time.toString().substr(0, 4) + " milliseconds"}</p>
                    <button onClick = {(e) => this.loadPerson()}> Press </button>
                    <table> 
                        <tr>
                            <th>ID</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>IP</th>
                            <th>Country</th>
                        </tr>
                    {persons.map(person => (
                            <tr key={person.id} align="start">
                                <td>{ person.id }</td>
                                <td>{ person.first_name }</td>
                                <td>{ person.last_name }</td>
                                <td>{ person.email }</td>
                                <td>{ person.gender }</td>
                                <td>{ person.ip_address }</td>
                                <td>{ person.country }</td>
                            </tr>
                    ))}
                    </table>
                </div>
            );
        }
    }
  }
  
  export default Person;
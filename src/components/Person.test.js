import React from 'react';
import { render } from '@testing-library/react';
import Person from './Person';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Data from './../assets/data.json'

Enzyme.configure({ adapter: new Adapter() });

test('Should create h1 text', () => {
  const { getByText } = render(<Person />);
  const linkElement = getByText("Load json");
  expect(linkElement).toBeInTheDocument();
});


it('Time should not be 0', () => {
  const wrapper = Enzyme.shallow(<Person />);
  const firstButton = wrapper.find('button').at(0);

  firstButton.simulate('click');
  setTimeout(() => {
    expect(wrapper.state()).time.not(0)
  }, 5000);
});

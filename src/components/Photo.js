import React from 'react';
class Photo extends React.Component {
    render(){
        return (
            <div >
                <h1> Photo </h1>
                <img src={require('./../assets/1.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/2.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/3.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/4.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/5.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/6.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/7.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/8.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/9.jfif')} width="100%" alt="foo"/>
                <img src={require('./../assets/10.jfif')} width="100%" alt="foo"/>
            </div>
        );
    }
}
export default Photo;
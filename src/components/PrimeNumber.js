import React from 'react';

class PrimeNumber extends React.Component {
    constructor() {
      super();
      this.state = {
        primeNumbers: String,
        time: 0
      };
    }
  
    handleChange = e => {
        let time = performance.now();
        this.primeNumbers = "";
        this.startTime = new Date(Date.now());
        let num = 20000;
    
        let isPrime = true;
        for (let i = 0; i <= num; i++)
        {
          for (let j = 2; j <= num; j++)
          {
            if (i !== j && i % j === 0) {
              isPrime = false;
              break;
            }
          }
          if (isPrime) {
            this.primeNumbers = this.primeNumbers + " , " + i;
          }
          isPrime = true;
        }
    

        this.setState({ text: this.primeNumbers, time: performance.now() - time });
      
    };
  
    render() {
      return (
        <div className="App">
          <h2>
            PRIME <b style={{ fontWeight: 600 }}>FINDER</b>
          </h2>
          <center>
            <button onClick={this.handleChange}>  
                Calculate the first 20000 prime numbers.
            </button>
            <div>{this.state.time.toString().substr(0, 4) + " milliseconds"}</div>
            <div>
            </div>
            <div className="numbers">
              {this.state.prime}
              {this.state.text}
            </div>
          </center>
        </div>
      );
    }
  }
export default PrimeNumber;

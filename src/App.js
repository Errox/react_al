import React from 'react';
import Nav from './Nav'; 
import Pi from './components/Pi'; 
import PrimeNumber from './components/PrimeNumber';
import Person from './components/Person';
import Photo from './components/Photo';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <Router> 
      <div className="App">
        <Nav/>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/Pi"  component={Pi}/>
          <Route path="/PrimeNumber" component={PrimeNumber} />
          <Route path="/Person" component={Person}/>>
          <Route path="/Photo" component={Photo}/>
        </Switch>
      </div>
    </Router>
  );
}

const Home = () => (
  <div>
    <h1> Home Page </h1>
  </div>
);


export default App;
